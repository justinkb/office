# Copyright 2008 Richard Brown <rbrown@exherbo.org>
# Distributed under the terms of the GNU General Purpose License v2

require gnome.org [ suffix="tar.xz" ] gsettings gtk-icon-cache freedesktop-desktop
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

SUMMARY="GNOME Office Spreadsheet"
HOMEPAGE="http://www.gnome.org/projects/gnumeric/"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
perl [[ description = [ add support for perl plugins ] ]]
python [[ description = [ add support for python plugins ] ]]
"

DEPENDENCIES="
    build:
        dev-doc/gtk-doc[>=1.32-r1]
        app-text/rarian
        dev-util/intltool[>=0.35.0]
        gnome-desktop/yelp-tools
        sys-devel/autoconf-archive
        sys-devel/bison
        virtual/pkg-config[>=0.18]
    build+run:
        office-libs/goffice:0.10[>=0.10.38]
        office-libs/libgsf:1[>=1.14.33]
        dev-libs/libxml2:2.0[>=2.4.12]
        dev-libs/glib:2[>=2.38]
        x11-libs/pango[>=1.24.0]
        x11-libs/gtk+:3[>=3.8.7]
        perl? ( dev-lang/perl:* )
        python? ( dev-lang/python:=[=2*]
                  gnome-bindings/pygobject:3[>=3.0.0][python_abis:2.7] )
"

# most tests are skipped
# the rest can’t work due to DConf settings required
# which may not be available
RESTRICT="test"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --without-gda
    --without-psiconv
    --without-paradox
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( perl python )

src_prepare() {
    edo intltoolize --force --automake

    # emulate ./autogen.sh script for po-functions/
    edo rm -f po-functions/Makefile.in.in
    edo cp po{,-functions}/Makefile.in.in
    edo sed -e '/^\(GETTEXT_PACKAGE\|subdir\) =/s/[ \t]*$/-functions/' \
            -e 's|$(srcdir)/LINGUAS|$(top_srcdir)/po/LINGUAS|g' \
            -e '/^GETTEXT_PACKAGE =/a\nXGETTEXT_KEYWORDS = --keyword --keyword=F_' \
            -e '/^EXTRA_DISTFILES/s/ LINGUAS//' \
            -i po-functions/Makefile.in.in

    # use target-prefixed pkg-config
    edo sed -i \
        -e '/GOFFICE_PLUGINS_DIR=/s/pkg-config/$PKG_CONFIG/' \
        -e '/GOFFICE_VERSION=/s/pkg-config/$PKG_CONFIG/' \
        configure.ac

    autotools_src_prepare
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

